extends Node2D

var tilemap
var tree_tilemap

export var spawn_area : Rect2 = Rect2(50, 150, 700, 700)
export var max_spawned = 40
export var start_spawned = 10
var spawned_count = 0

# This is the only skeleton-specific reference
var creature_to_spawn = preload("res://scenes/Skeleton.tscn")

var rng = RandomNumberGenerator.new()


func instance_creature():
	var creature = creature_to_spawn.instance()
	add_child(creature)
	
	# Place the skeleton in a valid position
	var valid_position = false
	while not valid_position:
		creature.position.x = spawn_area.position.x + rng.randf_range(0, spawn_area.size.x)
		creature.position.y = spawn_area.position.y + rng.randf_range(0, spawn_area.size.y)
		valid_position = test_position(creature.position)

	# Play skeleton's birth animation
	creature.arise()


func test_position(position : Vector2):
	# Check if the cell type in this position is grass or sand
	var cell_coord = tilemap.world_to_map(position)
	var cell_type_id = tilemap.get_cellv(cell_coord)
	var grass_or_sand = (cell_type_id == tilemap.tile_set.find_tile_by_name("Grass")) || (cell_type_id == tilemap.tile_set.find_tile_by_name("Sand"))
	
	# Check if there's a tree in this position
	cell_coord = tree_tilemap.world_to_map(position)
	cell_type_id = tree_tilemap.get_cellv(cell_coord)
	var no_trees = (cell_type_id != tilemap.tile_set.find_tile_by_name("Tree"))
	
	# If the two conditions are true, the position is valid
	return grass_or_sand and no_trees

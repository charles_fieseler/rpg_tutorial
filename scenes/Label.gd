extends Label




func _on_Skeleton_state_changed():
	var parent = get_parent()
	self.text = parent.movement_state.__str__() + " " + parent.attack_state.__str__()

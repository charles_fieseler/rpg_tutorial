extends DirectedKinematicBody2D

signal player_stats_changed

export var speed = 75
var attack_playing = false

var health_current = 100
var health_max = 100
var health_regen = 1
var mana_current = 100
var mana_max = 100
var mana_regen = 2

onready var sprite = $Sprite


func _ready():
	emit_signal("player_stats_changed", self)


func _process(delta):
	regen_health_and_mana(delta)
	

func regen_health_and_mana(delta):
	if mana_current < mana_max:
		mana_current = min(mana_current + mana_regen*delta, mana_max)
		emit_signal("player_stats_changed", self)
	
	if health_current < health_max:
		health_current = min(health_current + health_regen*delta, health_max)
		emit_signal("player_stats_changed", self)


func _input(event):
	# Handle attack animations
	if event.is_action_pressed("ui_attack"):
		attack_playing = true
		var animation = get_animation_str() + "_attack"
		$Sprite.play(animation)
	elif event.is_action_pressed("ui_special_attack"):
		if can_do_special_attack():
			mana_current -= 25
			emit_signal("player_stats_changed", self)
			attack_playing = true
			var animation = get_animation_str() + "_fireball"
			$Sprite.play(animation)
		

func can_do_special_attack():
	return mana_current >= 25


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _physics_process(delta):
	var direction = handle_direction_input()
		
	# Apply
	var movement = speed * direction * delta
	if attack_playing:
		movement *= 0.3
	move_and_collide(movement)
	
	if not attack_playing:
		animate_walking(direction)


func handle_direction_input():
	var direction: Vector2
	if Input.is_action_pressed("ui_left"):
		direction.x = -1
	elif Input.is_action_pressed("ui_right"):
		direction.x = 1
	if Input.is_action_pressed("ui_up"):
		direction.y = -1
	elif Input.is_action_pressed("ui_down"):
		direction.y = 1
	direction = direction.normalized()
	return direction


func animate_walking(direction: Vector2):
	if direction != Vector2.ZERO:
		# gradually update last_direction to counteract the bounce of the analog stick
		last_direction = 0.5 * last_direction + 0.5 * direction
		
		var animation_str = get_animation_str()
		$Sprite.play(animation_str + "_walk")
	else:
		
		var animation_str = get_animation_str()
		$Sprite.play(animation_str + "_idle")


func _on_Sprite_animation_finished():
	attack_playing = false

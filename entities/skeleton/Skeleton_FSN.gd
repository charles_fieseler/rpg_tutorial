extends DirectedFSN

var player

var rng = RandomNumberGenerator.new()

export var speed = 25
var bounce_countdown = 0

# Initialize FSN states
var all_states = {
	"IdleState": IdleState.new(),
	"WalkState": WalkState.new(),
	"AttackState": AttackState.new(),
	"SpecialAttackState": SpecialAttackState.new(),
	"EmptyState": State.new()
}

onready var sprite = $Sprite


func _ready():
	player = get_tree().root.get_node("Root/Player")
	rng.randomize()
	
	last_direction = Vector2(0,1)
	movement_state = all_states["IdleState"]
	attack_state = all_states["EmptyState"]


func _physics_process(delta):
	movement_state.handle_input(self, null)
	var collision = movement_state.update(self, delta)
	
	if collision != null and collision.collider.name != "Player":
		last_direction = last_direction.rotated(rng.randf_range(PI/4, PI/2))
		bounce_countdown = rng.randi_range(2, 5)
		
func handle_direction_input():
	# Always go in the same direction
	return last_direction
	
func handle_attack_input(event):
	return null


# The "brain"
func _on_Timer_timeout():
	var player_relative_position = player.position - position
	
	if player_relative_position.length() <= 16:
		# If player is near, don't move but turn toward it
		last_direction = player_relative_position.normalized()
		handle_state_transition(all_states["IdleState"])
		
	elif player_relative_position.length() <= 100 and bounce_countdown == 0:
		# If player is within range, move toward it
		last_direction = player_relative_position.normalized()
		if self.movement_state != all_states["WalkState"]:
			handle_state_transition(all_states["WalkState"])
#		movement_state.next_direction = last_direction
#		movement_state.handle_input(self, null)
		
	elif bounce_countdown == 0:
		# If player is too far, randomly decide whether to stand still or where to move
		var random_number = rng.randf()
		if random_number < 0.05:
			last_direction = Vector2.ZERO
			handle_state_transition(all_states["IdleState"])
		elif random_number < 0.1:
			last_direction = Vector2.DOWN.rotated(rng.randf() * 2 * PI)
			handle_state_transition(all_states["WalkState"])
#			all_states["WalkState"].next_direction = last_direction
#			movement_state.handle_input(self, null)
	
	# Update bounce countdown
	if bounce_countdown > 0:
		bounce_countdown = bounce_countdown - 1

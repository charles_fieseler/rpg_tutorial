extends KinematicBody2D

# Code for determining which direction an entity is facing
class_name DirectedKinematicBody2D

var last_direction = Vector2.ZERO


func get_animation_direction(direction: Vector2):
	# Y direction decides first
	if direction.y > 0.1:
		return "down"
	elif direction.y < -0.1:
		return "up"
	elif direction.x > 0.1:
		return "right"
	elif direction.x < -0.1:
		return "left"
	return "down"

# Must name the sprite correctly
func get_animation_str():
	# Parse the left/right sprites and properly flip	
	var animation_str = get_animation_direction(last_direction)
	$Sprite.flip_h = false
	if animation_str == "right":
		$Sprite.flip_h = true
		animation_str = "left"
	elif animation_str == "up":
		animation_str = "down"
	
	return animation_str


func update_last_direction(next_direction):
	last_direction = 0.5 * last_direction + 0.5 * next_direction


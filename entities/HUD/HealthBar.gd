extends ColorRect


var bar_max = 72

func _on_Player_player_stats_changed(var player):
	$Bar.rect_size.x = bar_max * player.health_current / player.health_max

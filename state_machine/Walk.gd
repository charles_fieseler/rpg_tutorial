extends State

class_name WalkState

var next_direction = Vector2.ZERO


func enter(agent: DirectedFSN):
	if not agent.is_animation_locked:
		var animation_str = agent.get_animation_str()
		agent.sprite.play(animation_str + "_walk")


func exit(agent: DirectedFSN):
	next_direction = Vector2.ZERO


func handle_input(agent: DirectedFSN, input):
	# Direction is handled in agent script
	next_direction = agent.handle_direction_input()
	if next_direction != Vector2.ZERO:
		next_state = null
	else:
		next_state = agent.all_states["IdleState"]
		
	# Attacks are also global, and handled by the agent
	var attack_type = agent.handle_attack_input(input)
	if attack_type:
		next_state = agent.all_states[attack_type]
		
	return next_state


func update(agent: DirectedFSN, delta):
	# gradually update last_direction to counteract the bounce of the analog stick
	agent.update_last_direction(next_direction)
	
	if not agent.is_animation_locked:
		var animation_str = agent.get_animation_str()
		agent.sprite.play(animation_str + "_walk")
	
	var movement = agent.speed * agent.last_direction * delta
	return agent.move_and_collide(movement)
	

func state_type():
	return "Movement"

func __str__():
	return "Walk"

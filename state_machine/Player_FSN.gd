extends DirectedFSN

signal player_stats_changed

export var speed = 75
#var attack_playing = false

var health_current = 100
var health_max = 100
var health_regen = 1
var mana_current = 100
var mana_max = 100
var mana_regen = 2

onready var sprite = $Sprite

# Initialize FSN states
var all_states = {
	"IdleState": IdleState.new(),
	"WalkState": WalkState.new(),
	"AttackState": AttackState.new(),
	"SpecialAttackState": SpecialAttackState.new(),
	"EmptyState": State.new()
}


func _ready():
	emit_signal("player_stats_changed", self)
	movement_state = all_states["IdleState"]
	attack_state = all_states["EmptyState"]


func _process(delta):
	regen_health_and_mana(delta)
	

func regen_health_and_mana(delta):
	if mana_current < mana_max:
		mana_current = min(mana_current + mana_regen*delta, mana_max)
		emit_signal("player_stats_changed", self)
	
	if health_current < health_max:
		health_current = min(health_current + health_regen*delta, health_max)
		emit_signal("player_stats_changed", self)


func _input(event):
	var next_state = null
	next_state = movement_state.handle_input(self, event)
	if next_state:
		handle_state_transition(next_state)
	next_state = attack_state.handle_input(self, event)
	if next_state:
		handle_state_transition(next_state)


#func handle_state_stack_transition(next_state: State):
##	print("Start")
##	print(movement_state)
#	if next_state.overrides_previous_state():
#		var last_state = state_stack.pop_back()
#		last_state.exit(self)
#
#	next_state.enter(self)
##	state_stack.append(next_state)
##	print("End")
##	print(state_stack)

#
#func revert_to_previous_state():
#	var last_state = state_stack.pop_back()
#	last_state.exit(self)
#
##	if len(state_stack) == 0:
##		state_stack.append(all_states["IdleState"])
#	state_stack[-1].enter(self)


func can_do_special_attack():
	return mana_current >= 25


func _physics_process(delta):
	movement_state.update(self, delta)


func handle_direction_input():
	# This ability is common to all states
	var direction: Vector2
	if Input.is_action_pressed("ui_left"):
		direction.x = -1
	elif Input.is_action_pressed("ui_right"):
		direction.x = 1
	if Input.is_action_pressed("ui_up"):
		direction.y = -1
	elif Input.is_action_pressed("ui_down"):
		direction.y = 1
	direction = direction.normalized()
	return direction


func handle_attack_input(event):
	# This ability is common to all states
	if event.is_action_pressed("ui_attack"):
		return "AttackState"
	elif event.is_action_pressed("ui_special_attack"):
		return "SpecialAttackState"
	else:
		return null



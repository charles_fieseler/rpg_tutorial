extends State

class_name AttackState

var original_speed


func enter(player: DirectedKinematicBody2D):
	if not player.is_animation_locked:
		var animation_str = player.get_animation_str()
		player.sprite.play(animation_str + "_attack")
	
		player.sprite.connect("animation_finished", self, "_finish_animation", [player])	
		player.is_animation_locked = true
		
		original_speed = player.speed
		player.speed = 35
		
	else:
		# Requires animation to be free
		# Partially redundent with can_interrupt()
		self._push_to_empty_state(player)

func _finish_animation(player):
	player.sprite.disconnect("animation_finished", self, "_finish_animation")
	
	player.is_animation_locked = false
	player.movement_state.enter(player) # To properly end animation
	self._push_to_empty_state(player)

func _push_to_empty_state(player):
	next_state = player.all_states["EmptyState"]
	player.handle_state_transition(next_state, "Attack", true)

func exit(player):
	player.speed = original_speed

func handle_input(player, input):
	return null

func update(player, delta):
	pass

func state_type():
	return "Attack"

func can_interrupt():
	return false

func __str__():
	return "Attack"

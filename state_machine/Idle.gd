extends State

class_name IdleState


func enter(agent):
	if not agent.is_animation_locked:
		var animation_str = agent.get_animation_str()
		agent.sprite.play(animation_str + "_idle")
#	agent.last_direction = Vector2.ZERO
	
func exit(agent):
	pass
	
func handle_input(agent, input):
	var direction = agent.handle_direction_input()

	if direction != Vector2.ZERO:
		# If we don't handle it *before* transition, it lags
		next_state = agent.all_states["WalkState"]
		next_state.handle_input(agent, input)
	else:
		next_state = null
		
	# Attacks are also global, and handled by the agent
	var attack_type = agent.handle_attack_input(input)
	if attack_type:
		next_state = agent.all_states[attack_type]
		
	return next_state

func update(agent, delta):
	pass

func state_type():
	return "Movement"

func __str__():
	return "Idle"

extends DirectedKinematicBody2D

class_name DirectedFSN

var movement_state: State
var attack_state: State

signal state_changed

# Assume the agent is animated
var is_animation_locked = false

func handle_state_transition(next_state: State, class_type=null, force_interrupt=false):
	# Can pass class type to force a transition, e.g. attack back to empty
	if not class_type:
		class_type = next_state.state_type()
		
	if class_type == "Movement":
		if movement_state.can_interrupt() or force_interrupt:
			movement_state.exit(self)
			next_state.enter(self)
			movement_state = next_state
			emit_signal("state_changed")
	elif class_type == "Attack":
		if attack_state.can_interrupt() or force_interrupt:
			attack_state.exit(self)
			next_state.enter(self)
			attack_state = next_state
			emit_signal("state_changed")
	else:
		print("Unable to transition to state:")
		print(next_state)
	
